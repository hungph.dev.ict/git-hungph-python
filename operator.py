print 10 + 20
print 20 - 10
print 10 * 2
print 10 / 2
print 10 % 3
print 2**3
print 10 // 3

print 10 < 20
print 10 > 20
print 10 <= 10
print 20 >= 15
print 5 == 6
print 5 != 6
print 5 <> 6

var = 10
var += 2
print var
var -= 2
print var
var *= 2
print var
var /= 2
print var
var %= 2
print var
var **= 2
print var
var //= 2
print var

a = 5>4 and 3>2
print a
b = 5>4 or 3<2
print b
c = not(5>4)
print c

a = 60            # 60 = 0011 1100 
b = 13            # 13 = 0000 1101 
c = 0

c = a & b;        # 12 = 0000 1100
print "Dong 1 - Gia tri cua c la ", c

c = a | b;        # 61 = 0011 1101 
print "Dong 2 - Gia tri cua c la ", c

c = a ^ b;        # 49 = 0011 0001
print "Dong 3 - Gia tri cua c la ", c

c = ~a;           # -61 = 1100 0011
print "Dong 4 - Gia tri cua c la ", c

c = a << 2;       # 240 = 1111 0000
print "Dong 5 - Gia tri cua c la ", c

c = a >> 2;       # 15 = 0000 1111
print "Dong 6 - Gia tri cua c la ", c

a = 20
b = 20
if( a is b):
    print  "a,b co cung identity"
else:
    print "a, b la khac nhau"
b = 10
if (a is not b):
    print  "a,b co identity khac nhau"
else:
    print "a,b co cung identity"